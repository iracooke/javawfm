
# Uncomment this and set it to your proxy address if needed
http_proxy=http://proxy.latrobe.edu.au:8080


if [[ -n $http_proxy ]]; then

	sudo touch /etc/apt/apt.conf
	sudo chmod o+w /etc/apt/apt.conf
	sudo echo "Acquire::http::Proxy \"$http_proxy\";" > /etc/apt/apt.conf
	sudo chmod o-w /etc/apt/apt.conf

	export http_proxy
	export https_proxy=$http_proxy

	echo "Sys.setenv(http_proxy=\"$http_proxy\")" > ~/.Rprofile

fi

sudo apt-get update
sudo apt-get install -y ant git r-base r-cran-rjava
sudo apt-get install -y openjdk-6-jdk

wget https://bitbucket.org/iracooke/javawfm/get/b5fcf7319f0a.zip

unzip b5fcf7319f0a.zip 

chmod -R u+w iracooke-javawfm-b5fcf7319f0a/

this_dir=`pwd`

cd iracooke-javawfm-b5fcf7319f0a/farmR/src/java
ant

cd $this_dir/iracooke-javawfm-b5fcf7319f0a/

sudo R CMD INSTALL farmR
