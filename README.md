## This repo combines two related projects, javawfm and farmR

javawfm is a java mixed integer modelling framework for arable farming systems.  It provides the underlying library used by farmR

farmR is the R interface to javawfm.  Unfortunately farmR is no longer available for easy install via CRAN but installation.

## Building

Getting farmR working can be tricky.  The easiest way is to use Vagrant and follow these instructions


1. Install [Virtualbox](https://www.virtualbox.org/) and [Vagrant](http://www.vagrantup.com/)

2. Clone this repository (you may need to setup ssh keys for bitbucket. Otherwise download the zip file of the source)

		git clone git@bitbucket.org:iracooke/javawfm.git

3. Edit `bootstrap.sh`.  In particular, you should consider editing the http_proxy setting if your institution uses a poorly configured proxy server like mine does

4. Start the virtual machine (this will take a while)

		cd javawfm
		vagrant up

5. Login to the newly created VM (farmR should be installed)

		vagrant ssh


## Using farmR

```R
	require(farmR)
	# Create a farm with default parameters
	fm=Farm()
	# Solve the farm model
	solvelp(fm)
	# Show basic information about the solution
	show(fm)
	# Extract key solution variables into an R data frame
	data.frame(fm)
```

To get a list of the available methods and classes type
```R
	help(package="farmR") 
```